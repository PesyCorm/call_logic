import time
import socket
from typing import Literal, Optional

from voip import media as _media_manager
from voip.new_SIP.sip_manager import SipFlow, SIPMessage, SIPStatus, SIPMessageType
from voip.implement_call import _Call, _CallAudioWrapper, _CallVideoWrapper, PhoneStatus


class _Phone:
    stt_workers_num = 1

    def __init__(
        self,
        call_type: Literal['Audio'] | Literal['Video'],
        pbx_host: str,
        pbx_port: int,
        username: str,
        password: str,
    ):
        self.call_type = call_type
        self.pbx_host = pbx_host
        self.pbx_port = pbx_port
        self.username = username
        self.password = password
        self._status: Optional[PhoneStatus] = None

        self.calls: dict[str, _Call] = dict()
        self._sip = SipFlow(
            self.pbx_host,
            self.pbx_port,
            username,
            password,
            self.__get_socket(connected=True),
            self._callback,
        )
        self._call_wrapper = _CallAudioWrapper if self.call_type == 'Audio' else _CallVideoWrapper
        self._media_manager = _media_manager.Audio if self.call_type == 'Audio' else _media_manager.Video
        self._media_manager.init_stt_workers(self.stt_workers_num)

    def _del_call(self, call_id: str) -> None:
        self.calls.pop(call_id)

    def _callback(self, request: SIPMessage) -> None:
        requested_call = self.calls[request.headers['Call-ID']]
        if request.type == SIPMessageType.MESSAGE:
            if request.method == "BYE":
                requested_call._handle_bye(request)
                #self._del_call(requested_call.call_data['call_id'])  # TODO not here
            else:
                raise RuntimeError(f'Unknown SIP message: {request.method}')
        else:
            if request.status == SIPStatus.TRYING:
                requested_call._handle_trying(request)
            elif request.status == SIPStatus.OK:
                requested_call._handle_OK(request)
            elif request.status == SIPStatus.NOT_FOUND:
                requested_call._handle_not_found(request)
            elif request.status == SIPStatus.SERVICE_UNAVAILABLE:
                requested_call._handle_unavailable(request)
            elif request.status == SIPStatus.UNAUTHORIZED:
                requested_call._handle_unauthorized(request)
            else:
                raise RuntimeError(f'Unknown sip status: {request.status}')

    def __get_socket(self, connected: bool = False) -> socket.socket:
        """Get available socket.
        :return: tuple[port, socket instance]
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(('', 0))
        if connected:
            sock.connect((self.pbx_host, self.pbx_port))
        return sock

    @property
    def status(self) -> PhoneStatus:
        return self._status

    def start(self) -> "_Phone":
        self._status = PhoneStatus.REGISTERING
        self._sip.start()
        self._status = PhoneStatus.REGISTERED
        return self

    def call(self, number: str = '') -> _CallAudioWrapper | _CallVideoWrapper:
        new_call = _CallAudioWrapper(
            pbx_info={'host': self.pbx_host, 'port': self.pbx_port},
            sip_manager=self._sip,
            media_manager=self._media_manager(self.__get_socket()),
        )
        new_call.new_call(number)
        self.calls[new_call.call_data['call_id']] = new_call
        return new_call

    def stop(self) -> None:
        if self.calls:
            for call_id in self.calls.keys():
                self._stop_call(call_id)
                #self.current_call.state in (CallState.DIALING, CallState.RINGING, CallState.ANSWERED)
        self._status = PhoneStatus.DEREGISTERING
        self._sip.stop()
        self._status = PhoneStatus.INACTIVE

    def _stop_call(self, call_id):
        if call := self.calls.get(call_id):
            call.hangup()
            self._del_call(call_id)


class AudioPhone(_Phone):
    def __init__(
        self,
        server: str,
        port: int,
        username: str,
        password: str
    ):
        super().__init__('Audio', server, port, username, password)


class VideoPhone(_Phone):
    def __init__(
        self,
        server: str,
        port: int,
        username: str,
        password: str
    ):
        super().__init__('Video', server, port, username, password)
