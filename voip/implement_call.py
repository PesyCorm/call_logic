import time

from voip import media as _media_manager
from voip.new_SIP.sip_manager import SipFlow as SIP
from voip.new_SIP.sip_factory import SIPMessage
from voip.data.ivr_indexes import *


class CallState(Enum):
    DIALING = "DIALING"
    RINGING = "RINGING"
    TRYING = "TRYING"
    ANSWERED = "ANSWERED"
    ENDED = "ENDED"


class CallStopReason(Enum):
    SIP_ERROR = "SIP_ERROR"
    BYE_FROM_PBX = "BYE_FROM_PBX"
    BYE_FROM_LOCAL = "BYE_FROM_LOCAL"


class PhoneStatus(Enum):
    INACTIVE = "INACTIVE"
    REGISTERING = "REGISTERING"
    REGISTERED = "REGISTERED"
    DEREGISTERING = "DEREGISTERING"
    FAILED = "FAILED"


class _Call:
    def __init__(
        self,
        pbx_info: dict,
        sip_manager: SIP,
        media_manager: _media_manager.Audio | _media_manager.Video
    ):
        self.sip_manager: SIP = sip_manager
        self.media_manager = media_manager

        self.call_data = dict()
        self.call_data['available_payload'] = media_manager.available_codecs
        self.call_data['media_port'] = self.media_manager.data['port']
        self.call_data['type'] = self.CALL_TYPE  # noqa
        self.call_data['state'] = None
        self.call_data['sendtype'] = _media_manager.TransmitType.SENDRECV
        self.call_data['pbx_info'] = pbx_info

    def new_call(self, number: str) -> None:
        if not self.call_data.get('call_id'):
            self.call_data['number'] = number
            new_call_data = self.sip_manager.send_invite(
                number, self.call_data['media_port'], self.call_data['sendtype'], self.call_data['available_payload']
            )
            self._parse_invite(new_call_data)
        else:
            raise Exception('Для оригинации нового звонка используй инстанс *Phone')

    # def __del__(self):  TODO
    #     self.hangup()

    def _parse_invite(self, request: SIPMessage) -> None:
        new_data = {
            'invite_request': request,
            'call_id': request.headers['Call-ID'],
            'sess_id': request.body['o']['id']
        }
        # Этого пока что хватит, можно докинуть при необходимости
        self.call_data.update(new_data)

    def _stop_media(self):
        self.media_manager.stop()

    def _stop(self):
        if self.call_data['state']:
            self._stop_media()
            if self.call_data['state'] in (CallState.TRYING, CallState.ANSWERED):
                self.sip_manager.send_bye(self.call_data['invite_request'])
                self.call_data['state'] = CallState.ENDED
                if self.call_data.get('stop_reason') != CallStopReason.BYE_FROM_LOCAL:
                    raise Exception(
                        'При остановке звонка по инициативе абонента А, должна быть указана корректная причина остановки.'
                    )

    def _check_leg_A_answered(self):
        st = time.time()
        while (time.time() - 10) < st:
            if self.call_data['state'] == CallState.ANSWERED:
                return
        raise RuntimeError('Не дозвонились до АТС')

    def _handle_trying(self, request: SIPMessage) -> None:
        self.call_data['state'] = CallState.TRYING
        self.sip_manager.send_ack(request)

    def _handle_unauthorized(self, request: SIPMessage) -> None:
        if not self.call_data.get('auth_sent'):
            self.sip_manager.send_ack(request)
            self.sip_manager.send_invite(
                self.call_data['number'],
                self.call_data['media_port'],
                self.call_data['sendtype'],
                self.call_data['available_payload'],
                self.call_data['sess_id'],
                request
            )
            self.call_data['auth_sent'] = True
        else:
            raise RuntimeError('Не смогли авторизовать звонок, дважды получен 401')

    def _handle_not_found(self, request: SIPMessage) -> None:
        self.call_data['stop_reason'] = CallStopReason.SIP_ERROR
        self.call_data['error'] = {'type': 'not found', 'request': request}
        self.sip_manager.send_ack(request)
        self._stop()

    def _handle_unavailable(self, request: SIPMessage) -> None:
        self.call_data['stop_reason'] = CallStopReason.SIP_ERROR
        self.call_data['error'] = {'type': 'unavailable', 'request': request}
        self.sip_manager.send_ack(request)
        self._stop()

    def _handle_OK(self, request: SIPMessage) -> None:
        if self.call_data['state'] == CallState.TRYING:
            self.call_data['state'] = CallState.ANSWERED
            media_data = (self.call_data['pbx_info']['host'], int(request.body['m'][0]['port']))
            print(f'update media socket: {media_data}')
            self.media_manager._set_socket_connection((self.call_data['pbx_info']['host'], request.body['m'][0]['port']))
            self.sip_manager.send_ack(request)
        elif self.call_data['state'] == CallState.ANSWERED:
            self.state = CallState.ENDED
        else:
            raise Exception('Получили 200 OK, но статус звонка не является DIALING/ANSWERED')

    def _handle_bye(self, request: SIPMessage):
        self.sip_manager.send_ok(request)
        self.call_data['stop_reason'] = CallStopReason.BYE_FROM_PBX
        self._stop()

    def hangup(self) -> None:
        self.call_data['stop_reason'] = CallStopReason.BYE_FROM_LOCAL
        self._stop()

    def send_audio(self, file_path: str):
        self._check_leg_A_answered()
        self.media_manager.send_audio(file_path)

    def send_dtmf(self, number: str):
        self._check_leg_A_answered()
        self.media_manager.send_dtmf(number)

    def listen(self, ivr_message) -> str:
        self._check_leg_A_answered()
        return self.media_manager.listen(ivr_message)


class _CallAudioWrapper(_Call):
    CALL_TYPE = 'Audio'

    def enter_redial(self):
        try:
            rd = self.listen(RedialIndex).split()
        except _media_manager.NoMatchesRecognize:
            # быстрый фикс, т.к. дорабатывать логику слишком долго.
            # такое может случиться, если перед вводом редиала нам предлагается вызвать дежурного.
            rd = self.listen(RedialIndex).split()
        assert len(rd) == 3, f'Неправильно распознали редиал.\nОжидалось: "Введите * *"\nПолучили: {rd}'
        self.send_dtmf(f'{rd[1]}{rd[2]}')

    def enter_pin(self, card_pin):
        self.listen(EnterPinIndex.ENTER_PIN)
        self.send_dtmf(card_pin)

    def enter_dst_number(self, phone_number):
        self.listen(EnterDestIndex.ENTER_DEST)
        self.send_dtmf(phone_number)


class _CallVideoWrapper(_Call):
    CALL_TYPE = 'Video'
